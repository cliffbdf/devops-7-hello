package hello.storytests;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import static org.junit.Assert.*;

// Import the Hello stub
import hello.stub.HelloFactory;
import hello.stub.HelloStub;

// Import the WeGood stub
import wegood.stub.WeGoodFactory;
import wegood.stub.WeGoodStub;

public class EdgeCasesIT {

	private String helloResponse = null;
	HelloStub hello = null;
	WeGoodStub wegood = null;

	public EdgeCasesIT() throws Exception {

		// Create stub for accessing the Hello service:
		String helloFactoryClassName = System.getenv("HELLO_FACTORY_CLASS_NAME");
	    HelloFactory helloFactory = (HelloFactory)
	        (Class.forName(helloFactoryClassName).getDeclaredConstructor().newInstance());
	    this.hello = helloFactory.createHello();

		// Create stub for accessing the WeGood service:
		String wegoodFactoryClassName = System.getenv("WEGOOD_FACTORY_CLASS_NAME");
	    WeGoodFactory weGoodFactory = (WeGoodFactory)
	        (Class.forName(wegoodFactoryClassName).getDeclaredConstructor().newInstance());
		this.wegood = weGoodFactory.createWeGood();
	}
}
