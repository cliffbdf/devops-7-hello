package hello.storytests;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import static org.junit.Assert.*;
import java.time.LocalTime;

// Import the Hello stub
import hello.stub.HelloFactory;
import hello.stub.HelloStub;

// Import the WeGood stub
import wegood.stub.WeGoodFactory;
import wegood.stub.WeGoodStub;

public class BasicIT {

	private String helloResponse = null;
	HelloStub hello = null;
	WeGoodStub wegood = null;

	public BasicIT() throws Exception {

		try {
			// Create stub for accessing the Hello service:
			String helloFactoryClassName = System.getenv("HELLO_FACTORY_CLASS_NAME");
			if (helloFactoryClassName == null) throw new Exception("Env variable HELLO_FACTORY_CLASS_NAME is not defined");
		    HelloFactory helloFactory = (HelloFactory)
		        (Class.forName(helloFactoryClassName).getDeclaredConstructor().newInstance());
		    this.hello = helloFactory.createHello();

			// Create stub for accessing the WeGood service:
			String wegoodFactoryClassName = System.getenv("WEGOOD_FACTORY_CLASS_NAME");
			if (wegoodFactoryClassName == null) throw new Exception("Env variable WEGOOD_FACTORY_CLASS_NAME is not defined");
			WeGoodFactory weGoodFactory = (WeGoodFactory)
				(Class.forName(wegoodFactoryClassName).getDeclaredConstructor().newInstance());
			this.wegood = weGoodFactory.createWeGood();
		} catch (Exception ex) {
			ex.printStackTrace(System.err);
			throw ex;
		}
	}

	@Given("the time is {string}")
	public void set_time(String timestr) throws Exception {

		try {
			// Parse the time string:
			LocalTime localTime = LocalTime.parse(timestr);

			/* Tell the WeGood service to set the time to a specified value. Note that as
				soon as we set it, it starts increasing: */
			this.wegood.setTime(localTime);

		} catch (Exception ex) {
			ex.printStackTrace(System.err);
			throw ex;
		}
	}

	@When("I call HelloWorld")
	public void call_hello_world() throws Exception {

		try {
	    	this.helloResponse = this.hello.hello();
		} catch (Exception ex) {
			ex.printStackTrace(System.err);
			throw ex;
		}
	}

	@Then("it returns {string}")
	public void it_returns(String expectedResponse) throws Exception {
		assertEquals(expectedResponse, this.helloResponse);
	}
}
