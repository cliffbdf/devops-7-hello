@component
Feature: Basic

	Scenario: Happy
		Given the time is "18:00"
		When I call HelloWorld
		Then it returns "Hello world!"

	Scenario: Unhappy
		Given the time is "09:00"
		When I call HelloWorld
		Then it returns "Goodbye"
