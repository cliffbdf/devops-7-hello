package hello.stub;
public class HelloMockFactory implements HelloFactory {
    public HelloStub createHello() {
        return new HelloMock();
    }
}
