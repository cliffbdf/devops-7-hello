package hello;

import static spark.Spark.*;
import wegood.stub.WeGoodFactory;
import wegood.stub.WeGoodStub;
import java.time.LocalTime;

public class HelloWorld {
	WeGoodStub weGood = null;

	public static void main(String[] args) throws Exception {
		HelloWorld helloWorld = new HelloWorld();
		get("/hello", (req, res) -> helloWorld.sayHi());
		get("/", (req, res) -> "Hello Service");
	}

	public HelloWorld() throws Exception {
		String weGoodFactoryClassName = System.getenv("WEGOOD_FACTORY_CLASS_NAME");
		if (weGoodFactoryClassName == null)
			throw new Exception("WEGOOD_FACTORY_CLASS_NAME not specified");
		WeGoodFactory factory = null;
		try {
			factory = (WeGoodFactory)(Class.forName(weGoodFactoryClassName)
				.getDeclaredConstructor().newInstance());
		} catch (Exception ex) {
			System.err.println(ex.getMessage());
			throw ex;
		}
		this.weGood = factory.createWeGood();
	}

	public String sayHi() throws Exception {
		try {

			/* debug

			String weGoodFactoryClassName2 = System.getenv("WEGOOD_FACTORY_CLASS_NAME");
			if ((weGoodFactoryClassName2 != null) && (! weGoodFactoryClassName2.equals("wegood.stub.WeGoodMockFactory"))) {
				// Obtain IP address of DNS server:
				// Ref: https://github.com/dnsjava/dnsjava
				java.util.List<java.net.InetSocketAddress> dnsServers = org.xbill.DNS.ResolverConfig.getCurrentConfig().servers();
				if (dnsServers == null) System.err.println("No DNS server list returned");
				else {
					if (dnsServers.size() == 0) System.err.println("No DNS servers found");
					for (java.net.InetSocketAddress server : dnsServers)
						System.err.println("DNS server: " + server.toString());
				}

				// Attempt DNS lookup of the wegood service:
				java.net.InetAddress wegoodip = null;
				try {
					wegoodip = java.net.InetAddress.getByName("wegood.moody.com");
					if (wegoodip == null)
						System.err.println("IP address of wegood.moody.com is unresolved");
					else
						System.err.println("IP address of wegood.moody.com is " + wegoodip.toString());
				} catch (Exception ex2) {
					System.err.println("Ex: IP address of wegood.moody.com is unresolved");
				}

				if (wegoodip != null) {

					String[] parts = wegoodip.toString().split("/");
					String wegoodipstr = parts[1];
					System.err.println("wegoodipstr is " + wegoodipstr);

					String wegoodurl = "http://" + wegoodipstr + ":80/";
					System.err.println("Making request to service at " + wegoodurl);
					try {
						String responseStr = cliffberg.utilities.HttpUtilities.getHttpResponseAsString(
							new java.net.URL(wegoodurl));
						System.err.println("WeGood response: " + responseStr);
					} catch (Exception ex3) {
						System.err.println("Unable to reach wegood at " + wegoodurl);
					}

					wegoodurl = "http://" + wegoodipstr + ":4567/";
					System.err.println("Making request to service at " + wegoodurl);
					try {
						String responseStr = cliffberg.utilities.HttpUtilities.getHttpResponseAsString(
							new java.net.URL(wegoodurl));
						System.err.println("WeGood response: " + responseStr);
					} catch (Exception ex3) {
						System.err.println("Unable to reach wegood at " + wegoodurl);
					}
				}
			}


			// end debug */

			boolean weGoodResponse = false;
			for (int i = 0; i < 3; i++) {
				System.err.println("Making request to service at " + weGood.getServiceURL());
				try { weGoodResponse = weGood.areWeGood(); } catch (Exception ex) {
					new Timer(1000, () -> {});  // wait one second
					if (i == 2) throw ex;  // too many times - re-throw the exception
					continue; // try again
				}
				System.err.println("Response received");
				break;
			}

	        if (weGoodResponse) return "Hello world!";
	        else return "Goodbye";

		} catch (Exception ex) {
			ex.printStackTrace(System.out);
			ex.printStackTrace(System.err);
			throw ex;
		}
	}

	/** For testing only */
	public void setTime(LocalTime time) throws Exception {
		this.weGood.setTime(time);
	}

	private class Timer {
		Thread t = new Thread();
		boolean isSleeping = false;
		Timer(int milliseconds, Runnable doIfInterrupted) {
			try {
				t.sleep(milliseconds);
				isSleeping = true;
				t.join();
			}
			catch (InterruptedException ex) { doIfInterrupted.run(); }
		}
		public void cancel() { t.interrupt(); isSleeping = false; }
		boolean isWaiting() { return this.isSleeping; }
	}
}
