# DevOps For Agile Coaches

Assignment for modules 15 & 16.

## Building Locally (on Workstation)

```
source env.local.source
./build.sh
```

## Building Locally (in a VM)

```
source env.vm.source
./build.sh
```

## Building In Jenkins

```
source env.jenkins.source
./build.sh <dockerhub-user-id> <dockerhub-password>
```

## Deploying and Component Testing Locally (in a VM)

```
source env.vm.source
source env.test.vm.source
./deploy.sh
```

## Deploying and Component Testing Locally (in a VM, under Compose)

```
source env.vm.source
source env.test.compose.source
./deploy.sh
```

## Deploying and Component Testing in Jenkins

```
source env.jenkins.source
source env.test.jenkins.source
./deploy.sh <dockerhub-user-id> <dockerhub-password>
```

## Deploying and Component Testing in an AWS ECS Cluster

```
source env.<test-env>.source
source env.test.cdk.source
./deploy.sh <dockerhub-user-id> <dockerhub-password>
```
